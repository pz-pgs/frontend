#!/bin/bash

set -e

SOURCE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function app_start() (
    echo "Starting frontend..."

    cd ${SOURCE_DIR}/../application
    yarn install && yarn build
    cd ..
    docker-compose -f frontend-compose.yml up -d

    cd -
    echo "Frontend started."
)

app_start
