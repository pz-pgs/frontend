# Live Streaming Web Application - Frontend

## Prerequisites

In order to run this application you need to install:
  - Node (version >= 10 atleast),
  - yarn,
  - Docker (when running in production mode).

## Available run possibilities:

### Development mode:

#### `cd scripts && ./start_dev.sh`

Runs the app in the development mode.<br />
The page will reload if you make edits.

### Production mode:

#### `cd scripts && ./start_prod.sh`

Runs dockerized version of the application <br />

## Access to GUI
Open [http://localhost:8081](http://localhost:8081) to view it in the browser.

## Running E2E tests:

In order to run E2E tests in your local environment:

1. Go to scripts/ directory.
2. Run install install_e2e_requirements.sh script, that install all the required modules that are used during tests.
3. Run run_e2e_tests.sh scripts, with optional arguments, such as:
   1. --due-to - time that tests are being run [s, m, h, d], i.e:

        #### `--due-to=5m`

   2. --test-filter - choose exact test suite, i.e:

        #### `--test-filter=login_page_e2e`

   3. --test-exclude - exclude exact test suite, i.e:

        #### `--test-exclude=login_page_e2e`

   4. --stop-on-fail - stops running tests when one test failed,
   5. --browser - browser that is being used to run tests [chrome(default), firefox], i.e:

        #### `--browser=firefox`
