import RequestService from 'common/services/RequestService';

export const roomDetails = roomId =>
  RequestService.get(`https://www.keycloak.test/api/room/${roomId}`);

export const roomLogin = (roomId, password) =>
  RequestService.post(`https://www.keycloak.test/api/room/${roomId}/join`, password);

export const roomRemove = roomId =>
  RequestService.delete(`https://www.keycloak.test/api/room/${roomId}`);
