import RequestService from 'common/services/RequestService';

export const getToken = sessionName =>
  RequestService.post(`https://www.keycloak.test/session/join`, sessionName);

export const leaveSession = data =>
  RequestService.post(`https://www.keycloak.test/session/leave`, data);
