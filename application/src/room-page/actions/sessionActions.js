import { getToken } from 'room-page/handlers/sessionHandler';
import { lockScreen, unlockScreen } from 'common/actions/lockScreenActions';

export const GET_TOKEN_FETCHING = 'GET_TOKEN_FETCHING';
export const GET_TOKEN_OK = 'GET_TOKEN_OK';
export const GET_TOKEN_FAIL = 'GET_TOKEN_FAIL';

export const makeGetTokenFetching = () => ({
  type: GET_TOKEN_FETCHING,
});

export const makeGetTokenOk = token => ({
  type: GET_TOKEN_OK,
  payload: {
    token,
  },
});

export const makeGetTokenFail = () => ({
  type: GET_TOKEN_FAIL,
});

export const fetchToken = data => dispatch => {
  dispatch(makeGetTokenFetching());
  dispatch(lockScreen());

  return getToken(data)
    .then(res => {
      dispatch(makeGetTokenOk(res.data));
    })
    .catch(() => {
      dispatch(makeGetTokenFail());
    })
    .finally(() => {
      dispatch(unlockScreen());
    });
};
