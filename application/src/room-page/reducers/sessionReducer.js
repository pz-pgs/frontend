import { GET_TOKEN_FETCHING, GET_TOKEN_OK, GET_TOKEN_FAIL } from 'room-page/actions/sessionActions';

const INITIAL_STATE = {
  data: '',
  mySessionId: '',
  session: undefined,
  mainStreamManager: undefined,
  publisher: undefined,
  subscribers: [],
  isFetching: false,
  fetchingError: false,
};

const tokenFetching = state => ({
  ...state,
  data: '',
  isFetching: true,
  fetchingError: false,
});

const tokenFail = state => ({
  ...state,
  data: '',
  isFetching: false,
  fetchingError: true,
});

const tokenOk = (state, action) => ({
  ...state,
  data: action.payload,
  fetchingError: false,
  isFetching: false,
});

export default (state, action) => {
  const stateDefinition = typeof state === 'undefined' ? INITIAL_STATE : state;
  switch (action.type) {
    case GET_TOKEN_OK:
      return tokenOk(stateDefinition, action);
    case GET_TOKEN_FETCHING:
      return tokenFail(stateDefinition);
    case GET_TOKEN_FAIL:
      return tokenFetching(stateDefinition);
    default:
      return stateDefinition;
  }
};
