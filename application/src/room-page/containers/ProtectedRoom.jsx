import React, { Component } from 'react';
import PropTypes from 'prop-types';
import privateRoomImg from 'images/private_room.svg';
import styled from 'styled-components';
import { loginToRoom } from 'room-page/actions/roomDetailsActions';
import { connect } from 'react-redux';
import ButtonProgressIndicator from 'common/components/ButtonProgressIndicator';

const ProtectedRoomWrapper = styled.div.attrs({ className: 'protected-room-wrapper' })`
  text-align: center;
`;
const PasswordButton = styled.button.attrs({ className: 'password-button' })`
  text-align: center;
  padding: 20px;
  width: 110px;
  border: none;
  background: #3f3d56;
  color: #fff;
  border-top-right-radius: 7px;
  border-bottom-right-radius: 7px;
  margin: 0;
  outline: none;
  cursor: pointer;

  &:disabled {
    opacity: 0.6;
  }

  &:disabled:hover {
    cursor: auto;
  }
`;

const ProtectedRoomImage = styled.img.attrs({
  className: 'protected-room-image',
  alt: 'protected-room-image',
})`
  margin: 0 auto;
  display: block;
  width: 500px;
`;

const PasswordInput = styled.input.attrs({ className: 'password-input' })`
  outline: none;
  padding: 20px 50px;
  margin: 0;
  border-top-left-radius: 7px;
  border-bottom-left-radius: 7px;
  border: none;
`;

const PageTitle = styled.p.attrs({ className: 'page-title' })`
  font-size: 24px;
  font-family: 'Sen', sans-serif;
  margin: 80px 0 20px 0;
`;

const ErrorBlock = styled.div.attrs({ className: 'error-blok' })`
  padding: 10px;
  text-align: center;
  background: #fce7e6;
  font-size: 12px;
  color: #552526;
  width: 40%;
  font-weight: bold;
  margin: 0 auto 10px auto;
`;

const PASSWORD_PLACEHOLDER = 'Enter password...';

const WRONG_PASSWORD_STYLE = {
  padding: '19px 50px',
  border: '1px solid red',
  borderRight: 'none',
};

class ProtectedRoom extends Component {
  state = {
    passwordText: '',
  };

  onPasswordChange = event => {
    const {
      target: { value },
    } = event;

    this.setState({ passwordText: value });
  };

  onEnter = e => {
    if (e.key === 'Enter') this.checkPassword();
  };

  checkPassword = () => {
    const newPassword = this.state.passwordText;
    const { loginToRoomFunc, roomId } = this.props;

    const objectToSend = {
      password: newPassword,
    };

    if (newPassword.length > 0) {
      loginToRoomFunc(roomId, objectToSend);
    }
  };

  render() {
    const { passwordText } = this.state;
    const { isLoginPending, isLoginError } = this.props;
    const sendButtonDisabled = this.state.passwordText.length < 1;

    return (
      <ProtectedRoomWrapper>
        <PageTitle>This room is protected with password</PageTitle>
        <ErrorBlock style={!isLoginError ? { visibility: 'hidden' } : {}}>
          Wrong password.
        </ErrorBlock>
        <ProtectedRoomImage src={privateRoomImg} />
        <PasswordInput
          style={isLoginError ? WRONG_PASSWORD_STYLE : {}}
          type="password"
          value={passwordText}
          onChange={this.onPasswordChange}
          placeholder={PASSWORD_PLACEHOLDER}
          onKeyDown={this.onEnter}
        />
        <PasswordButton disabled={sendButtonDisabled} onClick={this.checkPassword}>
          {isLoginPending ? <ButtonProgressIndicator /> : 'Join'}
        </PasswordButton>
      </ProtectedRoomWrapper>
    );
  }
}

ProtectedRoom.propTypes = {
  loginToRoomFunc: PropTypes.func.isRequired,
  roomId: PropTypes.string.isRequired,
  isLoginPending: PropTypes.bool.isRequired,
  isLoginError: PropTypes.bool.isRequired,
};
const mapStateToProps = state => ({
  roomId: state.session.roomDetails.roomId,
  isLoginError: state.session.roomDetails.isLoginError,
  isLoginPending: state.session.roomDetails.isLoginPending,
});

const mapDispatchToProps = dispatch => ({
  loginToRoomFunc: (roomId, password) => dispatch(loginToRoom(roomId, password)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProtectedRoom);
