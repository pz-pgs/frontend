/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import DetailsBox from 'room-page/components/DetailsBox';
import UserVideoComponent from 'room-page/components/UserVideoComponent';
import shortid from 'shortid';
import ProgressIndicatorCircular from 'common/components/ProgressIndicatorCircular';
import InitialSettings from 'room-page/components/InitialSettings';

const VideoComponentWrapper = styled.div.attrs({ className: 'video-component-wrapper' })`
  display: inline-block;
  font-family: 'Sen', sans-serif;
`;

const StreamComponentWrapper = styled.div.attrs({ className: 'stream-component-wrapper' })`
  overflow: hidden;
  max-height: 83vh;
  width: 100%;
  max-width: 100%;
  display: grid;
  grid-gap: 0;
  grid-template-rows: 22fr 2fr;
  grid-template-areas:
    'stream-box-area'
    'details-box-area';
  height: 100%;
`;

const VideoGrid = styled.div.attrs({ className: 'video-grid' })``;
const StreamBox = styled.div.attrs({ className: 'stream-box-area' })`
  grid: stream-box-area;
  max-height: 70vh;
`;

const MainVideoArea = styled.div.attrs({ className: 'main-video' })``;
const SmallVideoArea = styled.div.attrs({ className: 'small-video-area' })``;

class StreamComponent extends Component {
  render() {
    return (
      <StreamComponentWrapper id="session">
        {this.props.publisher === undefined ? (
          <InitialSettings
            onCheckboxChange={this.props.onCheckboxChange}
            initialAudio={this.props.initialAudio}
            initialVideo={this.props.initialVideo}
            joinSession={this.props.joinSession}
          />
        ) : null}
        <StreamBox
          id="main-video"
          className={this.props.fullScreenMode ? 'full-sc-video' : 'not-full'}
        >
          {!this.props.publisher && this.props.sessionStarted ? (
            <ProgressIndicatorCircular />
          ) : (
            <VideoGrid>
              <MainVideoArea>
                {this.props.mainStreamManager !== undefined ? (
                  <UserVideoComponent
                    isSharingScreen={this.props.isSharingScreen}
                    isSpeaking={this.props.isSpeaking}
                    bigVideo
                    hiddenCamera={!this.props.publishVideo}
                    streamManager={this.props.mainStreamManager}
                  />
                ) : null}
              </MainVideoArea>
              <SmallVideoArea id="stream-container">
                {this.props.publisher !== undefined ? (
                  <VideoComponentWrapper
                    className="stream-container small-video"
                    onClick={() => this.props.handleMainVideoStream(this.props.publisher)}
                  >
                    <UserVideoComponent
                      bigVideo={false}
                      hiddenCamera={!this.props.publishVideo}
                      streamManager={this.props.publisher}
                    />
                  </VideoComponentWrapper>
                ) : null}
                {this.props.subscribers.map(sub => (
                  <VideoComponentWrapper
                    key={shortid.generate()}
                    className="stream-container"
                    onClick={() => this.props.handleMainVideoStream(sub)}
                  >
                    <UserVideoComponent
                      bigVideo={false}
                      hiddenCamera={!this.props.publishVideo}
                      streamManager={sub}
                    />
                  </VideoComponentWrapper>
                ))}
              </SmallVideoArea>
            </VideoGrid>
          )}
        </StreamBox>

        <DetailsBox
          isSharingScreen={this.props.isSharingScreen}
          style={this.props.publisher !== undefined ? {} : { visibility: 'hidden' }}
          shareScreen={this.props.shareScreen}
          stopSharingScreen={this.props.stopSharingScreen}
          goFullScreen={this.props.goFullScreen}
          showVideo={this.props.showVideo}
          muteAudio={this.props.muteAudio}
          publishVideo={this.props.publishVideo}
          publishAudio={this.props.publishAudio}
        />
      </StreamComponentWrapper>
    );
  }
}

StreamComponent.propTypes = {
  onCheckboxChange: PropTypes.func.isRequired,
  joinSession: PropTypes.func.isRequired,
  stopSharingScreen: PropTypes.func.isRequired,
  goFullScreen: PropTypes.func.isRequired,
  shareScreen: PropTypes.func.isRequired,
  showVideo: PropTypes.func.isRequired,
  muteAudio: PropTypes.func.isRequired,
  getVideoClassName: PropTypes.func.isRequired,
  handleMainVideoStream: PropTypes.func.isRequired,
  initialAudio: PropTypes.bool.isRequired,
  initialVideo: PropTypes.bool.isRequired,
  sessionStarted: PropTypes.bool.isRequired,
  isSharingScreen: PropTypes.bool.isRequired,
  isSpeaking: PropTypes.bool.isRequired,
  publishVideo: PropTypes.bool.isRequired,
  publishAudio: PropTypes.bool.isRequired,
  fullScreenMode: PropTypes.bool.isRequired,
  publisher: PropTypes.instanceOf(Object).isRequired,
  mainStreamManager: PropTypes.instanceOf(Object).isRequired,
  subscribers: PropTypes.instanceOf(Array).isRequired,
};

export default StreamComponent;
