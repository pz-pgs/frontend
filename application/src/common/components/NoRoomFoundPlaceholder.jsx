import React from 'react';
import Placeholder from 'common/components/Placeholder';
import errorImage from 'images/not_found.svg';

const TITLE = 'No room found';
const SUBTITLE = 'No room was found with given id';
const ALT = 'No room was found with given id';
const CUSTOM_CLASS_NAME = 'no-room-found-placeholder';

const NoRoomFoundPlaceholder = () => (
  <Placeholder
    src={errorImage}
    alt={ALT}
    title={TITLE}
    subtitle={SUBTITLE}
    customClassName={CUSTOM_CLASS_NAME}
  />
);

export default NoRoomFoundPlaceholder;
