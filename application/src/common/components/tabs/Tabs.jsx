import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Tab from 'common/components/tabs/Tab';
import styled from 'styled-components';

const ProfileMenu = styled.ul.attrs({ className: 'profile-menu' })`
  list-style-type: none;
  margin: 0;
  padding: 0;
  border-bottom: 1px solid #f0f0f0;
  width: 100%;
`;

class Tabs extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Array).isRequired,
  };

  state = {
    activeTab: this.props.children[0].props.label,
  };

  onClickTabItem = tab => {
    this.setState({ activeTab: tab });
  };

  render() {
    const {
      onClickTabItem,
      props: { children },
      state: { activeTab },
    } = this;

    return (
      <div className="tabs">
        <ProfileMenu className="tab-list">
          {children.map(child => {
            const { label } = child.props;

            return <Tab activeTab={activeTab} key={label} label={label} onClick={onClickTabItem} />;
          })}
        </ProfileMenu>
        <div className="tab-content">
          {children.map(child => {
            if (child.props.label !== activeTab) return undefined;
            return child.props.children;
          })}
        </div>
      </div>
    );
  }
}

export default Tabs;
