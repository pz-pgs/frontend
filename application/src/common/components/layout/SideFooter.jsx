import React from 'react';
import styled from 'styled-components';

const SideFooterWrapper = styled.div.attrs({ className: 'side-footer-wrapper' })`
  border-top: 1px solid #eff1f6;
  position: absolute;
  bottom: 19px;
  color: #333;
  left: 0px;
  width: 100%;
  font-size: 11px;
  padding-top: 22px;
  text-align: center;
  text-transform: uppercase;
`;

function SideFooter() {
  return <SideFooterWrapper>&copy; 2020 tutowl.com</SideFooterWrapper>;
}

export default SideFooter;
