import React, { Component } from 'react';
import onClickOutside from 'react-onclickoutside';
import styled from 'styled-components';
import userImg from 'images/profile_img.jpg';
import triangleImg from 'images/triangleImg.png';
import eventDateIcon from 'images/icons/event_date_icon.svg';
import TimeAgo from 'javascript-time-ago';
import en from 'javascript-time-ago/locale/en';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

TimeAgo.addLocale(en);

const timeAgo = new TimeAgo('en-US');

const NotificationsMenu = styled.div.attrs({ className: 'notifications-menu' })`
  border: 1px solid rgba(100, 100, 100, 0.4);
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.25);
  background: #ffffff;
  line-height: 15px;
  font-size: 11px;
  position: absolute;
  top: 65px;
  left: -340px;
  z-index: 99;
  font-family: 'Roboto', sans-serif !important;
  cursor: auto;
`;

const NotificationsList = styled.ul.attrs({ className: 'notifications-list' })`
  list-style-type: none;
  margin: 0;
  padding: 0;
`;

const RelativeBox = styled.div.attrs({ className: 'relative-box' })`
  position: relative;
`;

const TopTriangle = styled.div.attrs({ className: 'top-triangle' })`
  background-image: url(${triangleImg});
  background-repeat: no-repeat;
  background-size: 33px 521px;
  background-position: 0 -305px;
  height: 11px;
  position: absolute;
  top: -11px;
  right: 10px;
  width: 20px;
`;

const NotificationListItem = styled.li.attrs({ className: 'notification-list-item' })`
  background: ${props => props.inputColor || '#fff'};
  border-bottom: 1px solid #f0f0f0;
  margin: 0;
  padding: 5px;
  cursor: pointer;
  min-width: 360px;
  display: grid;
  grid-gap: 10px;
  grid-template-columns: 1fr 8fr;
  grid-template-areas: 'notification-item-img notification-item-text';

  &:hover {
    background: #f4f4f4;
  }

  &:last-child {
    border-bottom: none;
  }
`;

const NotificationItemBox = styled.div.attrs({
  className: 'notification-item-box',
})`
  grid: notification-item-img;
  position: relative;
`;

const StyledLink = styled(Link).attrs({
  className: 'styled-notifications-list',
})`
  text-decoration: none;
  color: #000;
  display: inline-block;
`;

const NewNotificationDot = styled.div.attrs({
  className: 'new-notification-dot',
})`
  border-radius: 50%;
  position: absolute;
  top: 14px;
  left: -3px;
  width: 10px;
  height: 10px;
  background: #2d4564;
  border: 2px solid #f0f0f0;
`;

const NotificationItemImg = styled.img.attrs({
  className: 'notification-item-img',
  alt: 'notification-item-img',
})`
  align: left;
  width: 36px;
  border-radius: 50%;
  border: 1px solid #f0f0f0;
  cursor: pointer;
  margin: 3px;
`;

const NotificationTextName = styled.div.attrs({ className: 'notification-text-name' })`
  grid: notification-text-name;
  font-weight: 100;
  text-align: left;
`;

const BoldText = styled.span.attrs({ className: 'bold-top' })`
  font-weight: bold;
  margin-right: 4px;
`;

const NotificationsHeader = styled.p.attrs({ className: 'notifications-header' })`
  border-bottom: 1px solid #f0f0f0;
  margin: 0;
  padding: 10px;
  font-weight: bold;
`;

const NotificationsFooter = styled.p.attrs({ className: 'notifications-footer' })`
  border-top: 1px solid #f0f0f0;
  margin: 0;
  padding: 10px;
  font-weight: bold;
  text-align: center;
  cursor: pointer;

  &:hover {
    background: #f4f4f4;
    text-decoration: underline;
  }
`;

const NotificationDate = styled.p.attrs({ className: 'notification-date' })`
  margin: 0;
  padding: 0;
`;

const NotificationDateIcon = styled.img.attrs({
  className: 'notification-date-icon',
  alt: 'notification-date-icon',
})`
  width: 10px;
  display: inline-block;
  margin: 3px 3px 0 0;
  float: left;
`;

const LIST_NOTIFICATIONS = [
  {
    id: 0,
    author: 'John Doe',
    new: true,
    timestamp: '2020-04-29T02:50:00Z',
    content: "has just created a room 'Human brain' in category 'Biology'",
    link: '/',
  },
  {
    id: 1,
    author: 'Robin Scherbatsky',
    new: false,
    timestamp: '2020-04-29T01:02:00Z',
    content: "has just evaluated your homework in room 'Human Brain'. You got 'A'",
    link: '/',
  },
  {
    id: 2,
    author: 'John Doe',
    new: false,
    timestamp: '2020-03-29T07:30:00Z',
    content: "has just created a room 'Human brain' in category 'Biology'",
    link: '/',
  },
  {
    id: 3,
    author: 'John Doe',
    new: false,
    timestamp: '2020-03-29T07:30:00Z',
    content: "has just created a room 'Human brain' in category 'Biology'",
    link: '/',
  },
  {
    id: 4,
    author: 'John Doe',
    new: false,
    timestamp: '2020-03-29T07:30:00Z',
    content: "has just created a room 'Human brain' in category 'Biology'",
    link: '/',
  },
];

class NotificationDropdown extends Component {
  handleClickOutside = () => {
    this.props.onOutsideClick();
  };

  renderList = () => (
    <NotificationsList>
      {LIST_NOTIFICATIONS.map(notification => (
        <StyledLink to={notification.link} key={notification.id}>
          <NotificationListItem style={notification.new ? { background: '#f4f4f4' } : {}}>
            <NotificationItemBox>
              <NotificationItemImg src={userImg} />
              {notification.new && <NewNotificationDot />}
            </NotificationItemBox>
            <NotificationTextName>
              <BoldText>{notification.author}</BoldText>
              {notification.content}
              <NotificationDate>
                <NotificationDateIcon src={eventDateIcon} />
                {timeAgo.format(new Date(notification.timestamp))}
              </NotificationDate>
            </NotificationTextName>
          </NotificationListItem>
        </StyledLink>
      ))}
    </NotificationsList>
  );

  render() {
    return (
      <NotificationsMenu>
        <RelativeBox>
          <NotificationsHeader>Notifications</NotificationsHeader>
          {this.renderList()}
          <NotificationsFooter>Show all</NotificationsFooter>
          <TopTriangle />
        </RelativeBox>
      </NotificationsMenu>
    );
  }
}

NotificationDropdown.propTypes = {
  onOutsideClick: PropTypes.func.isRequired,
};

export default onClickOutside(NotificationDropdown);
