/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import RealmRolesService from 'common/authorization/RealmRolesService';
import NotificationDropdown from 'common/components/layout/NotificationDropdown';
import realmRoles from 'common/authorization/realmRoles';
import CurrentTime from 'common/components/layout/CurrentTime';
import ProfileCircle from 'common/components/layout/ProfileCircle';
import CreateRoomButton from 'common/components/layout/CreateRoomButton';
import VerticalLineRight from 'common/components/layout/VerticalLineRight';
import notificationIcon from 'images/icons/bell.svg';
import settingsIcon from 'images/icons/settings.svg';

const RightMenuWrapper = styled.div.attrs({ className: 'right-menu-wrapper' })`
  float: right;
  display: inline-block;
  height: 70px;
  line-height: 80px;
  font-size: 14px;
  font-family: 'Titillium Web', sans-serif;
  margin-right: 40px;
`;

const TopMenuWrapper = styled.ul.attrs({ className: 'top-menu-wrapper' })`
  margin: 0;
  padding: 0;
  list-style-type: none;
  display: inline-block;
  margin-right: 20px;
`;

const TopMenuItem = styled.li.attrs({ className: 'top-menu-item' })`
  margin: 0;
  padding: 0;
  cursor: pointer;
  margin-left: 15px;
  display: inline-block;
  position: relative;
`;

const NewNotifications = styled.div.attrs({ className: 'new-notifications' })`
  width: 8px;
  height: 8px;
  background: #2d4564;
  position: absolute;
  top: 24px;
  left: 11px;
  border-radius: 50%;
`;

const CreateRoomButtonWrapper = styled.div.attrs({ className: 'create-room-button-wrapper' })`
  display: inline-block;
  float: left;
`;

const TopMenuImg = styled.img.attrs({ className: 'top-menu-img', alt: 'top-menu-img' })`
  margin: 0;
  width: 20px;
  height: 20px;
`;

class RightMenu extends Component {
  state = {
    newNotifications: true,
    showNewNotoficationsDropDown: false,
  };

  onNotificationsClick = () => {
    this.setState({
      newNotifications: false,
      showNewNotoficationsDropDown: !this.state.showNewNotoficationsDropDown,
    });
  };

  showCreateRoomButton = () =>
    RealmRolesService.hasAnyRole([realmRoles.ROLE_LIVE_ADMIN, realmRoles.ROLE_LIVE_TEACHER]);

  render() {
    const { newNotifications, showNewNotoficationsDropDown } = this.state;

    return (
      <RightMenuWrapper>
        {this.showCreateRoomButton() ? (
          <CreateRoomButtonWrapper>
            <CreateRoomButton />
          </CreateRoomButtonWrapper>
        ) : null}
        <TopMenuWrapper>
          <TopMenuItem>
            <TopMenuImg
              src={notificationIcon}
              onClick={showNewNotoficationsDropDown ? null : this.onNotificationsClick}
            />
            {newNotifications && <NewNotifications />}
            {showNewNotoficationsDropDown && (
              <NotificationDropdown onOutsideClick={this.onNotificationsClick} />
            )}
          </TopMenuItem>
          <TopMenuItem>
            <Link to="/settings">
              <TopMenuImg src={settingsIcon} />
            </Link>
          </TopMenuItem>
        </TopMenuWrapper>
        <ProfileCircle onLogout={this.props.onLogout} username={this.props.username} />
        <VerticalLineRight />
        <CurrentTime />
        <VerticalLineRight />
      </RightMenuWrapper>
    );
  }
}

RightMenu.propTypes = {
  onLogout: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
};

export default RightMenu;
