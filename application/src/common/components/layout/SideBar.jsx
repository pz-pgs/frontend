/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import styled from 'styled-components';
import SideFooter from 'common/components/layout/SideFooter';
import IntegrateApps from 'common/components/layout/IntegrateApps';
import homeIcon from 'images/icons/home.svg';
import { Link } from 'react-router-dom';
import lessonIcon from 'images/icons/lesson.svg';
import resourcesIcon from 'images/icons/resources.svg';
import groupsIcon from 'images/icons/groups.svg';
import messagesIcon from 'images/icons/messages.svg';
import tasksIcon from 'images/icons/tasks.svg';
import calendarIcon from 'images/icons/calendar.svg';
import Colors from 'common/colors';

const SideBarWrapper = styled.div.attrs({ className: 'side-bar-header-wrapper' })`
  grid-area: side-bar-wrapper;
  background: ${Colors.WHITE};
  position: relative;
  border-right: 1px solid ${Colors.GALLERY};
  max-height: 90vh;
`;

const SideBarMenuWrapper = styled.ul.attrs({ className: 'sidebar-menu-wrapper' })`
  list-style-type: none;
  margin: 0;
  width: 100;
  padding: 0;
  font-family: 'Titillium Web', sans-serif;
`;

const SideBarMenuItemWrapper = styled.li.attrs({ className: 'sidebar-menu-item-wrapper' })`
  margin: 0;
  background: ${Colors.WHITE};
  border-right: none;
  text-align: center;
  font-size: 14px;
  padding: 15px 0;
  font-weight: bold;
  color: ${Colors.LIGHT_BLACK};

  &:first-child {
    border-top: none;
  }

  &:hover {
    cursor: pointer;
    color: #4369a2 !important;
    border-right: 3px solid #4369a2;
    background: #f3f7fd;
  }

  &:hover img {
    filter: invert(0.1) sepia(100%) hue-rotate(190deg) saturate(500%) !important;
  }
`;

const SideBarItemImg = styled.img.attrs({ className: 'sidebar-item-img', alt: 'sidebar-item-img' })`
  display: inline-block;
  margin: 0 auto;
  width: 24px;
  height: 24px;
  grid: img-area;
`;

const StyledLink = styled(Link).attrs({ className: 'sidebar-styled-link' })`
  font-size: 14px;
  line-height: 24px;
  font-weight: bold;
  text-decoration: none;
  display: grid;
  grid-template-columns: 1fr 2fr;
  line-height: 24px;
  grid-template-areas: 'img-area text-area';
`;

const SideBarItemText = styled.div.attrs({ className: 'sidebar-item-text' })`
  grid: text-area;
  text-align: left;
  font-size: 12px;
  color: #333;
`;

const MENU_ICONS = [
  {
    id: 0,
    link: '/',
    name: 'Overview',
    icon: homeIcon,
  },
  {
    id: 1,
    link: '/classes',
    name: 'Classes',
    icon: lessonIcon,
  },
  {
    id: 2,
    link: '/',
    name: 'Resources',
    icon: resourcesIcon,
  },
  {
    id: 3,
    link: '/calendar',
    name: 'Calendar',
    icon: calendarIcon,
  },
  {
    id: 4,
    link: '/',
    name: 'Tasks',
    icon: tasksIcon,
  },
  {
    id: 5,
    link: '/',
    name: 'Groups',
    icon: groupsIcon,
  },
  {
    id: 6,
    link: '/messages',
    name: 'Messages',
    icon: messagesIcon,
  },
];

const activeTabStyle = {
  cursor: 'pointer',
  color: '#4369a2',
  borderRight: '3px solid #4369a2',
  background: '#f3f7fd',
};

const activeImageStyle = {
  filter: 'filter: invert(0.1) sepia(100%) hue-rotate(190deg) saturate(500%) !important',
};

class SideBar extends Component {
  state = {
    activeTab: window.location.href.split('/').pop(),
  };

  compareStrings = (stringOne, stringTwo) => stringOne.toUpperCase() === stringTwo.toUpperCase();

  isActiveTab = item =>
    this.compareStrings(item.name, this.state.activeTab) ||
    (this.state.activeTab === '' && item.name === 'Overview') ||
    (window.location.href.includes('/room/') && item.name === 'Classes');

  renderMenuIcons = () => (
    <SideBarMenuWrapper>
      {MENU_ICONS.map(item => (
        <SideBarMenuItemWrapper key={item.id} style={this.isActiveTab(item) ? activeTabStyle : {}}>
          <StyledLink to={item.link}>
            <SideBarItemImg
              src={item.icon}
              style={this.isActiveTab(item) ? activeImageStyle : {}}
            />
            <SideBarItemText>{item.name}</SideBarItemText>
          </StyledLink>
        </SideBarMenuItemWrapper>
      ))}
    </SideBarMenuWrapper>
  );

  render() {
    return (
      <SideBarWrapper>
        {this.renderMenuIcons()}
        <IntegrateApps />
        <SideFooter />
      </SideBarWrapper>
    );
  }
}

export default SideBar;
