/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import headerLogo from 'images/main_logo.svg';

const HeaderLogoWrapper = styled.div.attrs({ className: 'header-logo-wrapper' })`
  grid: header-logo-wrapper;
  background: #ffffff;
  padding: 10px 5px 5px 10px;
  border-right: 1px solid #eff1f6;
  border-bottom: 1px solid #eff1f6;
`;

const HeaderLogoImage = styled.img.attrs({ className: 'header-logo-img', alt: 'header-logo-img' })`
  height: 60px;
  width: 100%;
  display: inline;
  margin: 0 auto;
`;

export default function HeaderLogo() {
  return (
    <HeaderLogoWrapper>
      <Link to="/">
        <HeaderLogoImage src={headerLogo} />
      </Link>
    </HeaderLogoWrapper>
  );
}
