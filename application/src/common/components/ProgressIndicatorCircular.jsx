import React from 'react';
import progressImage from 'images/circle_progress.svg';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ImageContainer = styled.img.attrs({
  className: 'image-container-progress',
  alt: 'image-container-progress',
})`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const ProgressIndicatorCircular = ({ size }) => (
  <ImageContainer style={{ height: `${size}px` }} src={progressImage} alt="progress-circular" />
);

ProgressIndicatorCircular.defaultProps = {
  size: 60,
};

ProgressIndicatorCircular.propTypes = {
  size: PropTypes.number,
};

export default ProgressIndicatorCircular;
