import React from 'react';
import styled from 'styled-components';
import Placeholder from 'common/components/Placeholder';
import errorImage from 'images/404_not_found.svg';
import { Link } from 'react-router-dom';

const TITLE = 'Sorry, the page you are looking for cannot be displayed';
const SUBTITLE = 'Following error occured: requested URL not found';
const ALT = 'Requested URL not found';
const CUSTOM_CLASS_NAME = 'not-found-error-placeholder';

const NotFoundWrapper = styled.div`
  padding-top: 60px;
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  color: #fff;
`;

const GoBackHomeButton = styled.button`
  padding: 20px 25px;
  background: #2d4564;
  border: 1px solid #f0f0f0;
  border-radius: 4px;
  font-size: 13px;
  font-weight: 100;
  color: #fff;
  display: block;
  margin: 0 auto;
  cursor: pointer;
  transition: 0.2s;

  &:hover {
    opacity: 0.8;
  }
`;

const NotFoundPage = () => (
  <NotFoundWrapper>
    <Placeholder
      src={errorImage}
      alt={ALT}
      title={TITLE}
      subtitle={SUBTITLE}
      customClassName={CUSTOM_CLASS_NAME}
    />
    <StyledLink to="/">
      <GoBackHomeButton>Back to Home</GoBackHomeButton>
    </StyledLink>
  </NotFoundWrapper>
);

export default NotFoundPage;
