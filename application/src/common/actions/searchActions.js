import searchMethod from 'common/handlers/searchHandler';

export const SEARCH_PENDING = 'SEARCH_PENDING';
export const SEARCH_OK = 'SEARCH_OK';
export const SEARCH_FAIL = 'SEARCH_FAIL';

export const makeSearchPending = () => ({
  type: SEARCH_PENDING,
});

export const makeSearchOk = searchResult => ({
  type: SEARCH_OK,
  payload: { searchResult },
});

export const makeSearchFail = () => ({
  type: SEARCH_FAIL,
});

export const searchByQueryText = searchQuery => dispatch => {
  dispatch(makeSearchPending());

  return searchMethod(searchQuery)
    .then(res => {
      dispatch(makeSearchOk(res.data));
    })
    .catch(() => {
      dispatch(makeSearchFail());
    });
};
