import RequestService from 'common/services/RequestService';

export default searchQuery =>
  RequestService.get(`https://www.keycloak.test/api/search?searchQuery=${searchQuery}`);
