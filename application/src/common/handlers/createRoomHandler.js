import RequestService from 'common/services/RequestService';

export const createNewRoom = newRoomData =>
  RequestService.post(`https://www.keycloak.test/api/room/`, newRoomData);

export const checkIfNameExist = roomName =>
  RequestService.get(`https://www.keycloak.test/api/room/${roomName}/exist`);

export const getCategories = () =>
  RequestService.get(`https://www.keycloak.test/api/room/category/`);
