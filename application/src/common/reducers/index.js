import { combineReducers } from 'redux';
import lockScreenReducer from 'common/reducers/lockScreenReducer';
import authUserReducer from 'common/reducers/authUserReducer';
import createRoomReducer from 'common/reducers/createRoomReducer';
import searchReducer from 'common/reducers/searchReducer';

export default combineReducers({
  applicationScreen: lockScreenReducer,
  authUser: authUserReducer,
  createRoom: createRoomReducer,
  search: searchReducer,
});
