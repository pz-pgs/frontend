export default {
  url: 'https://www.keycloak.test/auth/',
  realm: 'master',
  clientId: 'frontend',
  onLoad: 'login-required',
};
