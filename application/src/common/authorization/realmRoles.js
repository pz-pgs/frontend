const ROLE_LIVE_ADMIN = 'live-admin';
const ROLE_LIVE_TEACHER = 'live-teacher';
const ROLE_LIVE_STUDENT = 'live-student';

export default {
  ROLE_LIVE_ADMIN,
  ROLE_LIVE_TEACHER,
  ROLE_LIVE_STUDENT,
};
