import { store } from 'store';
import get from 'lodash/get';

export default class RealmRolesService {
  static getRoles() {
    return get(store.getState(), 'common.authUser.keycloakInfo.realmAccess.roles', []);
  }

  static hasRole(role) {
    return RealmRolesService.getRoles().includes(role);
  }

  static hasAnyRole(roles) {
    return RealmRolesService.getRoles().some(role => roles.includes(role));
  }
}
