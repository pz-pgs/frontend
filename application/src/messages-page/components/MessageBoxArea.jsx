import React from 'react';
import styled from 'styled-components';
import MessagesProfileInfoArea from 'messages-page/components/MessagesProfileInfoArea';
import MessagesMainArea from 'messages-page/components/MessagesMainArea';
import userImg from 'images/profile_img.jpg';
import moreIcon from 'images/icons/more.svg';

const MessageBoxAreaWrapper = styled.div.attrs({ className: 'message-box-area' })`
  grid: message-box-area;
`;

const ProfileMoreIcon = styled.img.attrs({
  className: 'profile-more-icon',
  alt: 'profile-more-icon',
})`
  width: 14px;
  height: 14px;
  cursor: pointer;
  display: inline-block;
  margin-top: 15px;
  float: right;
`;

const MessageTopBar = styled.div.attrs({ className: 'message-top-bar' })`
  padding: 12px 40px;
  border-bottom: 1px solid #f0f0f0;
  background: #ffffff;
  position: relative;
`;
const UserImage = styled.img.attrs({ className: 'user-image', alt: 'user-image' })`
  height: 36px;
  width: 36px;
  position: absolute;
  top: 15px;
  left: 30px;
  border-radius: 50%;
`;

const TopBarUserName = styled.span.attrs({ className: 'top-bar-user-name' })`
  display: inline-block;
  font-size: 15px;
  margin: 0;
  line-height: 40px;
  margin-left: 40px;
`;

const UserActiveDot = styled.div.attrs({ className: 'user-active-dot' })`
  border-radius: 50%;
  width: 8px;
  height: 8px;
  bottom: 4px;
  background: #5bc247;
  margin-left: 5px;
  border: 2px solid #ffffff;
  right: -4px;
  display: inline-block;
`;

const MessagesBox = styled.div.attrs({ className: 'messages-box' })`
  display: grid;
  grid-template-areas: 'messages-main-area messages-profile-info-area';
  grid-template-columns: 8fr 3fr;
  height: 100%;
`;

const MESSAGES_ARRAY = [
  {
    id: 0,
    author: 'Amanda',
    time: '18:30',
    message: 'Elo byczku co tam u cb slychac, dawno nie bylo w00deczi hehe',
    image: userImg,
  },
  {
    id: 1,
    author: 'Amanda',
    time: '18:31',
    message: 'zubruweczka juz sie chlodzi mordeckzo',
    image: userImg,
  },
  {
    id: 2,
    author: '',
    time: '18:33',
    message: 'siemaa, dobra to wpadam tylko starej powiem ze ide na ryby',
    image: null,
  },
  {
    id: 3,
    author: 'Amanda',
    time: '18:34',
    message: 'dobra',
    image: userImg,
  },
  {
    id: 4,
    author: 'Amanda',
    time: '18:34',
    message: 'o kt bedziesz?',
    image: userImg,
  },
  {
    id: 5,
    author: '',
    time: '18:36',
    message: 'opierdole rybke i wsiadam w ubera do cb',
    image: null,
  },
  {
    id: 6,
    author: 'Amanda',
    time: '18:40',
    message: 'dobra to czekam, dawaj',
    image: userImg,
  },
  {
    id: 7,
    author: '',
    time: '19:33',
    message: 'ktora to byla piwnica',
    image: null,
  },
  {
    id: 8,
    author: '',
    time: '19:34',
    message: 'bo nie pamiteam w sumie',
    image: null,
  },
];

const MessageBoxArea = () => (
  <MessageBoxAreaWrapper>
    <MessageTopBar>
      <UserImage src={userImg} />
      <TopBarUserName>Amanda Green</TopBarUserName>
      <UserActiveDot />
      <ProfileMoreIcon src={moreIcon} />
    </MessageTopBar>
    <MessagesBox>
      <MessagesMainArea messagesArray={MESSAGES_ARRAY} />
      <MessagesProfileInfoArea />
    </MessagesBox>
  </MessageBoxAreaWrapper>
);

export default MessageBoxArea;
