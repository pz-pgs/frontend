import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const MessageItemWrapper = styled.div.attrs({ className: 'message-item-wrapper' })`
  display: grid;
  grid-template-columns: 1fr 10fr;
  grid-template-areas: 'message-user-image-area message-details-area';
  padding: 15px;
  font-family: 'Titillium Web', sans-serif;
  border-bottom: 1px solid #f0f0f0;
  cursor: pointer;
  transition: 0.15s;

  &:last-child {
    border-bottom: none;
  }

  &:hover {
    background: #f4f4f4;
  }
`;

const MessageTopInfo = styled.div.attrs({ className: 'message-top-info' })`
  margin: 3px 0 0 10px;
  display: grid;
  grid-template-columns: 5fr 1fr;
  grid-template-areas: 'message-user-name-area message-user-time-area';
`;

const MessageUserImageArea = styled.div.attrs({ className: 'message-user-image-area' })`
  grid: message-user-image-area;
  position: relative;
`;

const MessageDetailsArea = styled.div.attrs({ className: 'message-details-area' })`
  grid: message-details-area;
`;

const MessageUserImg = styled.img.attrs({ className: 'message-user-img', alt: 'message-user-img' })`
  border-radius: 50%;
  width: 45px;
  height: 45px;
  border: 1px solid #f0f0f0;
`;

const MessageUserImageActive = styled.div.attrs({ className: 'message-user-image-active' })`
  border-radius: 50%;
  width: 8px;
  height: 8px;
  bottom: 6px;
  background: #5bc247;
  border: 2px solid #ffffff;
  right: 0;
  position: absolute;
`;

const MessageUserName = styled.p.attrs({ className: 'message-user-name' })`
  grid: message-user-name-area;
  font-size: 13px;
  margin: 0;
  color: #4f6073;
  font-weight: 400;
`;

const MessageTime = styled.p.attrs({ className: 'message-user-name' })`
  grid: message-user-time-area;
  font-size: 13px;
  margin: 0;
  text-align: right;
  color: #cacfd5;
  font-weight: 300;
`;

const MessageComponent = styled.div.attrs({ className: 'message-component' })`
  font-size: 10px;
  color: #b4bbc3;
  margin: 5px 0 0 0;
  font-weight: 400;
  margin-left: 10px;
  display: grid;
  grid-template-columns: 8fr 1fr;
  grid-template-areas: 'message-content-area message-unred-area';
`;

const MessageContentArea = styled.div.attrs({ className: 'message-content-area' })`
  grid: message-content-area;
`;

const MessageUnreadArea = styled.div.attrs({ className: 'message-unread-area' })`
  grid: message-unread-area;
  text-align: right;
`;

const MessagesCount = styled.p.attrs({ className: 'messages-count' })`
  background: #d43939;
  border-radius: 50%;
  color: #ffffff;
  padding: 1px 5px;
  font-size: 9px;
  margin: 0;
  display: inline-block;
`;

const UNREAD_MESSAGE_STYLE = {
  background: '#f3f7fd',
};

const MessageItem = ({ messageObject }) => (
  <MessageItemWrapper style={messageObject.unread === 0 ? {} : UNREAD_MESSAGE_STYLE}>
    <MessageUserImageArea>
      <MessageUserImg src={messageObject.image} />
      {messageObject.active ? <MessageUserImageActive /> : null}
    </MessageUserImageArea>
    <MessageDetailsArea>
      <MessageTopInfo>
        <MessageUserName>{messageObject.author}</MessageUserName>
        <MessageTime>{messageObject.time}</MessageTime>
      </MessageTopInfo>
      <MessageComponent>
        <MessageContentArea> {messageObject.message}</MessageContentArea>
        <MessageUnreadArea>
          <MessagesCount style={messageObject.unread === 0 ? { visibility: 'hidden' } : {}}>
            {messageObject.unread}
          </MessagesCount>
        </MessageUnreadArea>
      </MessageComponent>
    </MessageDetailsArea>
  </MessageItemWrapper>
);

MessageItem.propTypes = {
  messageObject: PropTypes.objectOf(Object).isRequired,
};

export default MessageItem;
