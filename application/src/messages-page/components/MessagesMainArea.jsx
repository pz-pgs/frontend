import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import MessageMainInput from 'messages-page/components/MessageMainInput';
import MessagesMainComponent from 'messages-page/components/MessagesMainComponent';

const MessagesMainAreaWrapper = styled.div.attrs({ className: 'messages-main-area-wrapper' })`
  grid: messages-main-area;
  position: relative;
  max-height: 82vh;
`;

class MessagesMainArea extends Component {
  state = {
    messages: this.props.messagesArray,
  };

  sendMessage = message => {
    const newMessages = this.state.messages;
    newMessages.push(message);

    this.setState({
      messages: newMessages,
    });
  };

  render() {
    const { messages } = this.state;
    return (
      <MessagesMainAreaWrapper>
        <MessagesMainComponent messagesArray={messages} />
        <MessageMainInput onSend={this.sendMessage} />
      </MessagesMainAreaWrapper>
    );
  }
}

MessagesMainArea.propTypes = {
  messagesArray: PropTypes.arrayOf(Object).isRequired,
};

export default MessagesMainArea;
