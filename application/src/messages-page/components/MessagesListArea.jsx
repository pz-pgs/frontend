import React, { Component } from 'react';
import styled from 'styled-components';
import MessageSearchInput from 'messages-page/components/MessageSearchInput';
import MessageItem from 'messages-page/components/MessageItem';
import profileUserZero from 'images/profile_img.jpg';
import profileUserOne from 'images/users/profile_image_1.jpg';
import profileUserTwo from 'images/users/profile_image_2.jpg';
import profileUserThree from 'images/users/profile_image_3.png';
import profileUserFour from 'images/users/profile_image_4.jpg';

const MessagesListAreaWrapper = styled.div.attrs({ className: 'messages-list-area' })`
  grid: messages-list-area;
  background: #fff;
  border-right: 1px solid #f0f0f0;
`;

const MessagesList = styled.div.attrs({ className: 'messages-list' })`
  margin-top: 10px;
  max-height: 82vh;
  overflow: hidden;

  &:hover {
    overflow-y: scroll;
  }
`;

const MESSAGES_ARRAY = [
  {
    id: 0,
    author: 'Amanda Green',
    image: profileUserZero,
    time: '20:30',
    message: 'Looking forward to hearing from...',
    unread: 2,
    active: true,
  },
  {
    id: 1,
    author: 'Jay Cutler',
    image: profileUserOne,
    time: '19:59',
    message: 'Why you not on gym? Wtf bro???',
    unread: 5,
    active: false,
  },
  {
    id: 2,
    author: 'Twoja m00ma',
    image: profileUserTwo,
    time: '19:30',
    message: 'Hey zjedz ziemiacznki gosciu!',
    unread: 0,
    active: true,
  },
  {
    id: 3,
    author: 'Kubus Puchatek',
    image: profileUserThree,
    time: '18:23',
    message: 'W stumilowym lesie, cie goni...',
    unread: 3,
    active: true,
  },
  {
    id: 4,
    author: 'Ridley Scott',
    image: profileUserFour,
    time: '08:32',
    message: 'XD nice1 m8',
    unread: 0,
    active: false,
  },
  {
    id: 5,
    author: 'Britney Spears',
    image: profileUserTwo,
    time: '05:23',
    message: 'hit me baby pls',
    unread: 0,
    active: false,
  },
  {
    id: 6,
    author: 'Tadeusz Kosciuszko',
    image: profileUserOne,
    time: '04:23',
    message: 'na kon byku',
    unread: 0,
    active: true,
  },
  {
    id: 7,
    author: 'Andrzej Zolnierek',
    image: profileUserThree,
    time: '02:03',
    message: 'Tylko fejsbuki ehh',
    unread: 0,
    active: false,
  },
];

class MessagesListArea extends Component {
  state = {
    messagesArray: MESSAGES_ARRAY,
  };

  onFilter = filter => {
    if (filter === '') {
      this.setState({
        messagesArray: MESSAGES_ARRAY,
      });
    } else {
      const filteredArray = this.state.messagesArray.filter(message =>
        message.author.toLowerCase().includes(filter.toLowerCase()),
      );

      this.setState({
        messagesArray: filteredArray,
      });
    }
  };

  renderMessages = () => (
    <MessagesList>
      {this.state.messagesArray.map(message => (
        <MessageItem key={message.id} messageObject={message} />
      ))}
    </MessagesList>
  );

  render() {
    return (
      <MessagesListAreaWrapper>
        <MessageSearchInput onFilter={this.onFilter} />
        {this.renderMessages()}
      </MessagesListAreaWrapper>
    );
  }
}

export default MessagesListArea;
