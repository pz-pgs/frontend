import React from 'react';
import styled from 'styled-components';
import userImg from 'images/profile_img.jpg';
import phoneIcon from 'images/icons/phone.svg';
import mailIcon from 'images/icons/mail.svg';
import pinIcon from 'images/icons/pin.svg';
import profileUserOne from 'images/users/profile_image_1.jpg';
import profileUserTwo from 'images/users/profile_image_2.jpg';
import profileUserFour from 'images/users/profile_image_4.jpg';

const MessagesProfileInfoAreaWrapper = styled.div.attrs({ className: 'message-box-area' })`
  grid: messages-profile-info-area;
  background: #ffffff;
  border-left: 1px solid #f0f0f0;
  font-family: 'Titillium Web', sans-serif;
  z-index: 2;
  max-height: 82vh;
  overflow: hidden;

  &:hover {
    overflow-y: scroll;
  }
`;

const ImageBoxComponent = styled.img.attrs({
  className: 'image-box-component',
  alt: 'image-box-component',
})`
  width: 100px;
  height: 100px;
  margin: 0 auto;
  border-radius: 50%;
  display: block;
  margin-bottom: 10px;
  cursor: pointer;
`;

const BlockName = styled.p.attrs({ className: 'block-name' })`
  color: #b0b7c0;
  font-size: 16px;
  margin: 0 0 15px 0;
`;

const ProfileUsername = styled.p.attrs({ className: 'profile-username' })`
  color: #364a60;
  font-size: 18px;
  margin: 0;
`;

const InfoItem = styled.div.attrs({ className: 'info-item' })`
  display: grid;
  grid-template-columns: 1fr 7fr;
  grid-template-areas: 'info-item-icon info-item-text';
`;

const InfoItemIcon = styled.div.attrs({ className: 'info-item-icon' })`
  grid: info-item-icon;
  text-align: left;
`;

const InfoItemIconImage = styled.img.attrs({
  className: 'info-item-icon-image',
  alt: 'info-item-icon-image',
})`
  width: 16px;
  height: 16px;
`;

const InfoItemText = styled.div.attrs({ className: 'info-item-text' })`
  grid: info-item-text;
  font-size: 12px;
  color: #a8b1ba;
`;

const ProfileUserPosition = styled.p.attrs({ className: 'profile-user-position' })`
  color: #a8b1ba;
  font-size: 13px;
  margin: 0;
`;

const MediaList = styled.ul.attrs({ className: 'media-list' })`
  list-style-type: none;
  margin: 0;
  padding: 0;
`;

const MediaListItem = styled.li.attrs({ className: 'media-list-item' })`
  list-style-type: none;
  margin: 0;
  padding: 0;
  display: inline-block;
  height: 55px;
  width: 55px;
  margin-right: 15px;
  cursor: pointer;
`;

const MediaListItemImage = styled.img.attrs({
  className: 'media-list-item-image',
  alt: 'media-list-item-image',
})`
  width: 100%;
  height: 100%;
  border-radius: 8px;
  border: 1px solid #f0f0f0;
`;

const ImageBox = styled.div.attrs({ className: 'image-box' })`
  text-align: center;
  border-bottom: 1px solid #f0f0f0;
  padding: 20px;
`;

const InfoBlock = styled.div.attrs({ className: 'info-block' })`
  border-bottom: 1px solid #f0f0f0;
  padding: 20px;
`;

const MessagesProfileInfoArea = () => (
  <MessagesProfileInfoAreaWrapper>
    <ImageBox>
      <ImageBoxComponent src={userImg} />
      <ProfileUsername>Amanda Green</ProfileUsername>
      <ProfileUserPosition>PGS Software</ProfileUserPosition>
    </ImageBox>
    <InfoBlock>
      <BlockName>Information</BlockName>
      <InfoItem>
        <InfoItemIcon>
          <InfoItemIconImage src={pinIcon} />
        </InfoItemIcon>
        <InfoItemText>San Francisco, CA</InfoItemText>
      </InfoItem>
      <InfoItem>
        <InfoItemIcon>
          <InfoItemIconImage src={phoneIcon} />
        </InfoItemIcon>
        <InfoItemText>+1 (416) 235-6232</InfoItemText>
      </InfoItem>
      <InfoItem>
        <InfoItemIcon>
          <InfoItemIconImage src={mailIcon} />
        </InfoItemIcon>
        <InfoItemText>amanda.green@tutowl.com</InfoItemText>
      </InfoItem>
    </InfoBlock>
    <InfoBlock>
      <BlockName>Media (8)</BlockName>
      <MediaList>
        <MediaListItem>
          <MediaListItemImage src={userImg} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={profileUserOne} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={profileUserTwo} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={userImg} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={profileUserFour} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={profileUserTwo} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={profileUserOne} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={profileUserTwo} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={profileUserFour} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={userImg} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={userImg} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={userImg} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={profileUserOne} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={profileUserTwo} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={profileUserFour} />
        </MediaListItem>
        <MediaListItem>
          <MediaListItemImage src={profileUserFour} />
        </MediaListItem>
      </MediaList>
    </InfoBlock>
  </MessagesProfileInfoAreaWrapper>
);

export default MessagesProfileInfoArea;
