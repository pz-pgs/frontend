import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { animateScroll } from 'react-scroll';

const MessagesMainComponentWrapper = styled.div.attrs({
  className: 'messages-main-component-wrapper',
})`
  position: relative;
  height: 72vh;
  width: 100%;
  font-family: 'Titillium Web', sans-serif;
`;

const MessagesList = styled.ul.attrs({ className: 'messages-list' })`
  list-style-type: none;
  margin: 0;
  padding: 20px;
  position: absolute;
  max-height: 67vh;
  width: 95%;
  font-size: 13px;
  bottom: 0;

  overflow: hidden;

  &:hover {
    overflow-y: scroll;
  }
`;

const MessagesListItemRight = styled.li.attrs({ className: 'messages-list-item-right' })`
  display: block;
  max-width: 50%;
  text-align: right;
  float: right;
  clear: both;
  margin: 0 0 10px 0;
`;

const MessagesListItemLeft = styled.li.attrs({ className: 'messages-list-item-left' })`
  display: block;
  max-width: 60%;
  text-align: left;
  clear: both;
  float: left;
  margin: 0 0 10px 0;
  display: grid;
  grid-template-columns: 1fr 8fr;
  grid-template-areas: 'message-image-box-area message-content-area';
`;

const MessageImageBox = styled.div.attrs({ className: 'messages-image-box' })`
  grid: message-image-box-area;
  width: 44px;
`;

const MessageContentArea = styled.div.attrs({ className: 'messages-image-box' })`
  grid: message-content-area;
`;

const MessageAuthorParagraph = styled.div.attrs({ className: 'message-author-paragraph' })`
  margin: 0 0 5px 0;
  font-size: 9px;
`;

const MessageContent = styled.p.attrs({ className: 'messages-content' })`
  background: #ffffff;
  border: 1px solid #f0f0f0;
  padding: 10px;
  border-radius: 10px;
  max-width: 80%;
  margin: 0;
  border-top-left-radius: 0;
  display: inline-block;
`;

const MessageUserImage = styled.img.attrs({
  className: 'message-user-image',
  alt: 'message-user-image',
})`
  border-radius: 50%;
  width: 32px;
  width: 32px;
  border: 1px solid #f0f0f0;
  margin-right: 10px;
  cursor: pointer;
`;

class MessagesMainComponent extends Component {
  componentDidMount() {
    animateScroll.scrollToBottom({
      containerId: 'messages-list',
      duration: 200,
    });
  }

  componentDidUpdate() {
    animateScroll.scrollToBottom({
      containerId: 'messages-list',
      duration: 100,
    });
  }

  renderMessages = () => (
    <MessagesList id="messages-list">
      {this.props.messagesArray.map((item, i, arr) => {
        const previousItem = arr[i - 1];
        if (item.author === '') {
          return (
            <MessagesListItemRight key={item.id}>
              {i !== 0 && previousItem.author === item.author ? null : (
                <MessageAuthorParagraph>{item.time}</MessageAuthorParagraph>
              )}
              <MessageContent style={{ background: '#cbe1fe' }}>{item.message}</MessageContent>
            </MessagesListItemRight>
          );
        }
        return (
          <MessagesListItemLeft key={item.id}>
            <MessageImageBox>
              {i !== 0 && previousItem.author === item.author ? null : (
                <MessageUserImage src={item.image} />
              )}
            </MessageImageBox>
            <MessageContentArea>
              {i !== 0 && previousItem.author === item.author ? null : (
                <MessageAuthorParagraph>
                  {item.author}, {item.time}
                </MessageAuthorParagraph>
              )}
              <MessageContent
                style={i !== 0 && previousItem.author === item.author ? { marginLeft: '0' } : {}}
              >
                {item.message}
              </MessageContent>
            </MessageContentArea>
          </MessagesListItemLeft>
        );
      })}
    </MessagesList>
  );

  render() {
    return <MessagesMainComponentWrapper>{this.renderMessages()}</MessagesMainComponentWrapper>;
  }
}

MessagesMainComponent.propTypes = {
  messagesArray: PropTypes.arrayOf(Object).isRequired,
};

export default MessagesMainComponent;
