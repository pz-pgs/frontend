import RequestService from 'common/services/RequestService';

export default () => RequestService.get(`https://www.keycloak.test/api/room/calendar`);
