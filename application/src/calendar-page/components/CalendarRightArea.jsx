import React from 'react';
import styled from 'styled-components';
import CalendarUserInfo from 'calendar-page/components/CalendarUserInfo';

const CalendarRightAreaWrapper = styled.div.attrs({ className: 'calendar-right-area-wrapper' })`
  grid: calendar-right-area;
  background: #ffffff;
  border: 1px solid #f0f0f0;
  padding: 20px;
`;

const CalendarRightArea = () => (
  <CalendarRightAreaWrapper>
    <CalendarUserInfo />
  </CalendarRightAreaWrapper>
);

export default CalendarRightArea;
