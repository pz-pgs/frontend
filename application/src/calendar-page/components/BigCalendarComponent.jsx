import React, { Component } from 'react';
import styled from 'styled-components';
import FullCalendar from '@fullcalendar/react';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import PropTypes from 'prop-types';
import withLoading, { ProgIndSize } from 'common/utils/withLoading';
import CalendarEventModal from 'calendar-page/components/CalendarEventModal';
import { connect } from 'react-redux';
import { fetchCalendarEvents } from 'calendar-page/actions/bigCalendarActions';

const BigCalendarComponentWrapper = styled.div.attrs({
  className: 'big-calendar-component-wrapper',
})`
  background: #ffffff;
  font-size: 12px;
  padding: 25px;
`;

const BigCalendarComponentWrapperWithLoading = withLoading(
  BigCalendarComponentWrapper,
  ProgIndSize.XX_LARGE,
);

const format = {
  hour: 'numeric',
  minute: '2-digit',
  omitZeroMinute: false,
  meridiem: 'short',
};

class BigCalendarComponent extends Component {
  state = {
    event: undefined,
    showModal: false,
  };

  componentWillMount() {
    this.props.fetchCalendarEventsFunc();
  }

  handleEventClick = ({ event }) => {
    this.setState({ event }, () => {
      this.handleModal();
    });
  };

  handleModal = () => {
    this.setState({ showModal: !this.state.showModal });
  };

  render() {
    const { isError, isLoading, eventsList } = this.props;
    const { showModal, event } = this.state;

    if (isError) {
      return 'nie dziala';
    }

    return (
      <BigCalendarComponentWrapperWithLoading isLoading={isLoading}>
        <FullCalendar
          nowIndicator
          weekNumberCalculation="ISO"
          editable
          aspectRatio="2.1"
          locale="en-gb"
          defaultView="timeGridWeek"
          plugins={[timeGridPlugin, interactionPlugin]}
          events={eventsList}
          eventClick={this.handleEventClick}
          slotLabelFormat={format}
          minTime="04:00:00"
          maxTime="22:00:00"
        />
        {showModal && <CalendarEventModal onClose={this.handleModal} event={event} />}
      </BigCalendarComponentWrapperWithLoading>
    );
  }
}

BigCalendarComponent.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  fetchCalendarEventsFunc: PropTypes.func.isRequired,
  eventsList: PropTypes.instanceOf(Array).isRequired,
};

const mapStateToProps = state => ({
  isLoading: state.calendar.calendarEvents.isLoading,
  isError: state.calendar.calendarEvents.isError,
  eventsList: state.calendar.calendarEvents.events,
});

const mapDispatchToProps = dispatch => ({
  fetchCalendarEventsFunc: () => dispatch(fetchCalendarEvents()),
});

export default connect(mapStateToProps, mapDispatchToProps)(BigCalendarComponent);
