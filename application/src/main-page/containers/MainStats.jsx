import React from 'react';
import styled from 'styled-components';
import Colors from 'common/colors';
import WelcomeBackBadge from 'main-page/components/WelcomeBackBadge';
import ScheduleArea from 'main-page/components/ScheduleArea';
import StatsSideBarArea from 'main-page/components/StatsSideBarArea';

const MainStatsWrapper = styled.div.attrs({ className: 'main-stats-wrapper' })`
  grid: main-stats-area;
  background: ${Colors.BACKGROUND_COLOR};
  padding: 40px 50px;
`;

const MainStatsGridArea = styled.div.attrs({ className: 'main-stats-grid-area' })`
  display: grid;
  padding: 20px 0 0 0;
  grid-template-columns: 1fr 1fr;
  grid-gap: 20px;
  grid-template-areas: 'schedule-area stats-side-bar-area';
  height: 59vh;
  overflow: hidden;
  max-height: 59vh;
`;

const MainStats = () => (
  <MainStatsWrapper>
    <WelcomeBackBadge />
    <MainStatsGridArea>
      <ScheduleArea />
      <StatsSideBarArea />
    </MainStatsGridArea>
  </MainStatsWrapper>
);

export default MainStats;
