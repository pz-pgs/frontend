import React from 'react';
import styled from 'styled-components';
import Colors from 'common/colors';
import UserInfoHeader from 'main-page/components/UserInfoHeader';
import UpcomingEvents from 'main-page/components/UpcomingEvents';
import SmallCalendar from 'main-page/components/SmallCalendar';

const UserInfoWrapper = styled.div.attrs({ className: 'user-info-wrapper' })`
  grid: user-info-area;
  background: ${Colors.WHITE};
  border-left: 1px solid ${Colors.GALLERY};
  padding: 30px;
`;

const UserDescription = styled.p.attrs({ className: 'user-description' })`
  padding-top: 10px;
  border-top: 2px solid #f4f4f4;
  font-size: 13px;
  font-family: 'Titillium Web', sans-serif;
`;

const UserInfo = () => (
  <UserInfoWrapper>
    <UserInfoHeader />
    <UserDescription>
      Hi! My name is Amanda Green. I was born and raised in Chicago, but i dont like NBA. Im
      concerned about Computer Science!
    </UserDescription>
    <UpcomingEvents />
    <SmallCalendar />
  </UserInfoWrapper>
);

export default UserInfo;
