import React from 'react';
import styled from 'styled-components';
import { Line } from 'react-chartjs-2';

const TopStatsAreaWrapper = styled.div.attrs({ className: 'top-stats-area-wrapper' })`
  grid: top-stats-area;
  background: #ffffff;
  padding: 20px;
  border: 1px solid #f0f0f0;
`;

const CardTitle = styled.p.attrs({ className: 'card-title' })`
  font-size: 14px;
  font-family: 'Heebo', sans-serif;
  margin: 0 0 5px 0;
  font-weight: 100;
`;

const CHART_DATA = {
  labels: ['22 Apr', '23 Apr', '24 Apr', '25 Apr', '26 Apr'],
  datasets: [
    {
      label: 'Count',
      data: [1, 6, 2, 0, 12],
      backgroundColor: 'rgba(229, 243, 253, 0.3)',
      borderColor: '#2e4663',
      borderWidth: 1,
    },
  ],
};

const TopStatsArea = () => (
  <TopStatsAreaWrapper>
    <CardTitle>Finished homeworks per day in last week</CardTitle>
    <Line
      data={CHART_DATA}
      options={{
        responsive: true,
        maintainAspectRatio: true,
        legend: {
          display: false,
        },
      }}
    />
  </TopStatsAreaWrapper>
);

export default TopStatsArea;
