import React from 'react';
import styled from 'styled-components';
import userImg from 'images/profile_img.jpg';

const UserInfoHeaderWrapper = styled.div.attrs({ className: 'user-info-header-wrapper' })`
  display: grid;
  grid-template-columns: 1fr 3fr;
  grid-gap: 20px;
  font-family: 'Titillium Web', sans-serif;
  grid-template-areas: 'user-info-image-area user-info-text-area;';
`;

const UserInfoImageWrapper = styled.div.attrs({ className: 'user-info-image-wrapper' })`
  grid: user-info-image-area;
`;

const UserInfoTextWrapper = styled.div.attrs({ className: 'user-info-text-wrapper' })`
  grid: user-info-text-area;
`;

const UserInfoImage = styled.img.attrs({ className: 'user-info-image', alt: 'user-info-image' })`
  width: 64px;
  height: 64px;
  border-radius: 5px;
  border: 2px solid #f4f4f4;
`;

const UserName = styled.p.attrs({ className: 'user-name' })`
  font-weight: bold;
  font-size: 14px;
  margin: 8px 0 3px 0;
`;

const UserStatus = styled.p.attrs({ className: 'user-status' })`
  font-weight: 300;
  font-size: 12px;
  margin: 0;
`;

export default function UserInfoHeader() {
  return (
    <UserInfoHeaderWrapper>
      <UserInfoImageWrapper>
        <UserInfoImage src={userImg} />
      </UserInfoImageWrapper>
      <UserInfoTextWrapper>
        <UserName>Amanda Green</UserName>
        <UserStatus>PGS Software</UserStatus>
      </UserInfoTextWrapper>
    </UserInfoHeaderWrapper>
  );
}
