import React, { Component } from 'react';
import styled from 'styled-components';
import cameraIcon from 'images/icons/camera.svg';
import ChangeProfileImageModal from 'profile-page/components/ChangeProfileImageModal';
import userImg from 'images/profile_img.jpg';
import LinkToProfile from 'profile-page/components/LinkToProfile';

const ProfileSettingInfoWrapper = styled.div.attrs({ className: 'profile-page-info-wrapper' })`
  border: 1px solid #f0f0f0;
  grid: profile-settings-info-area;
  background: #ffffff;
  border-radius: 3px;
  z-index: 2;
  font-family: 'Titillium Web', sans-serif;
`;

const CameraIcon = styled.img.attrs({ className: 'camera-icon', alt: 'camera-icon' })`
  height: 16px;
  width: 16px;
  filter: invert(0.9);
`;

const CameraIconBox = styled.div.attrs({ className: 'camera-icon-box' })`
  background: #2d4564;
  border-radius: 50%;
  position: absolute;
  bottom: 5px;
  padding: 5px 7px 2px 7px;
  right: 50px;
  border: 5px solid #ffffff;
  cursor: pointer;
`;

const InfoList = styled.ul.attrs({ className: 'info-list' })`
  list-style-type: none;
  margin: 20px 0 0 0;
  padding: 0;
`;

const InfoListItem = styled.li.attrs({ className: 'info-list-item' })`
  margin: 0;
  padding: 15px;
  color: #545e79;
  border-top: 1px solid #f0f0f0;
  font-size: 12px;
  display: grid;
  grid-template-columns: 3fr 1fr;
  grid-template-areas: 'info-list-left info-list-right';

  &:last-child {
    border-bottom: 1px solid #f0f0f0;
  }
`;

const InfoListLeft = styled.div.attrs({ className: 'info-list-left' })`
  grid: info-list-left;
  text-align: left;
`;

const InfoListRight = styled.div.attrs({ className: 'info-list-right' })`
  grid: info-list-right;
  text-align: right;
  color: #333f60;
  font-weight: bold;
`;

const ProfileImageComponent = styled.img.attrs({
  className: 'profile-image-component',
  alt: 'profile-image-component',
})`
  width: 120px;
  height: 120px;
  border: 1px solid #f0f0f0;
  border-radius: 50%;
`;

const ProfileImageBox = styled.div.attrs({ className: 'profile-image-box' })`
  text-align: center;
  position: relative;
  padding: 15px 40px 0 40px;
`;

const ProfileName = styled.p.attrs({ className: 'profile-name' })`
  font-size: 16px;
  text-align: center;
  margin: 20px 0 0 0;
  color: #333f60;
  font-weight: bold;
`;

const ViewProfileButton = styled.button.attrs({ className: 'view-profile-button' })`
  border-radius: 4px;
  backgorund: none;
  border: 1px solid #f0f0f0;
  font-size: 12px;
  text-align: center;
  transition: 0.2s;
  display: block;
  margin: 10px auto 0 auto;
  width: 87%;
  outline: none;
  cursor: pointer;
  padding: 10px;

  &:hover {
    background: #f0f0f0;
  }
`;

const ProfileCompany = styled.p.attrs({ className: 'profile-name' })`
  font-size: 13px;
  text-align: center;
  font-weight: 100;
  color: #b2bcc5;
  margin: 0;
`;

const LIST_INFO = [
  {
    id: 0,
    name: 'Attented live streams:',
    value: 29,
  },
  {
    id: 1,
    name: 'Created groups:',
    value: 5,
  },
  {
    id: 2,
    name: 'Private messages:',
    value: 420,
  },
];

class ProfileSettingsInfo extends Component {
  state = {
    changeImageModal: false,
  };

  showChangImageModal = () => (
    <ChangeProfileImageModal
      onClose={() => {
        this.setState({ changeImageModal: false });
      }}
      onSave={() => {
        this.setState({ changeImageModal: false });
      }}
    />
  );

  clickModal = () => {
    this.setState({
      changeImageModal: !this.state.changeImageModal,
    });
  };

  renderList = () => (
    <InfoList>
      {LIST_INFO.map(info => (
        <InfoListItem key={info.id}>
          <InfoListLeft>{info.name}</InfoListLeft>
          <InfoListRight>{info.value}</InfoListRight>
        </InfoListItem>
      ))}
    </InfoList>
  );

  render() {
    const { changeImageModal } = this.state;

    return (
      <ProfileSettingInfoWrapper>
        <ProfileImageBox>
          <ProfileImageComponent src={userImg} />
          <CameraIconBox onClick={this.clickModal}>
            <CameraIcon src={cameraIcon} />
          </CameraIconBox>
        </ProfileImageBox>
        <ProfileName>Amanda Green</ProfileName>
        <ProfileCompany>PGS Software</ProfileCompany>
        {this.renderList()}
        {changeImageModal && this.showChangImageModal()}
        <ViewProfileButton>View Public Profile</ViewProfileButton>
        <LinkToProfile link="http://tutowl.com/profile/123dsd2de2e2" />
      </ProfileSettingInfoWrapper>
    );
  }
}

export default ProfileSettingsInfo;
