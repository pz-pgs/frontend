import React from 'react';
import styled from 'styled-components';
import ToggleSwitch from 'common/components/ToggleSwitch';
import UpdateButton from 'profile-page/components/UpdateButton';

const ProfilePrivacyWrapper = styled.div.attrs({
  className: 'profile-privacy-wrapper',
})``;

const ProfileGridWrapper = styled.div.attrs({ className: 'profile-grid-wrapper' })`
  padding: 0 20px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-areas: 'profile-form-left profile-form-right';
`;

const ProfileFormLeft = styled.div.attrs({ className: 'profile-form-left' })`
  grid: profile-form-left;
`;

const ProfileFormRight = styled.div.attrs({ className: 'profile-form-right' })`
  grid: profile-form-right;
`;

const PrivacyOption = styled.p.attrs({ className: 'privacy options' })`
  font-size: 13px;
  color: #59637d;
  margin: 0;
  padding: 30px 0;
  border-bottom: 1px solid #f0f0f0;

  &:last-child {
    border-bottom: none;
  }
`;

const ProfilePrivacy = () => (
  <ProfilePrivacyWrapper>
    <ProfileGridWrapper>
      <ProfileFormLeft>
        <PrivacyOption>Allow eveyone to see your profile</PrivacyOption>
        <PrivacyOption>Allow the app to use your location</PrivacyOption>
        <PrivacyOption>Allow the people to find you by your phone number</PrivacyOption>
        <PrivacyOption>Allow everyone to send you private message</PrivacyOption>
      </ProfileFormLeft>
      <ProfileFormRight>
        <PrivacyOption>
          <ToggleSwitch />
        </PrivacyOption>
        <PrivacyOption>
          <ToggleSwitch />
        </PrivacyOption>
        <PrivacyOption>
          <ToggleSwitch />
        </PrivacyOption>
        <PrivacyOption>
          <ToggleSwitch />
        </PrivacyOption>
      </ProfileFormRight>
    </ProfileGridWrapper>

    <UpdateButton style={{ marginTop: '53px' }} />
  </ProfilePrivacyWrapper>
);

export default ProfilePrivacy;
