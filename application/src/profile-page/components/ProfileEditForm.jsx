import React from 'react';
import styled from 'styled-components';
import TextInput from 'common/components/TextInput';
import SelectInput from 'common/components/SelectInput';
import UpdateButton from 'profile-page/components/UpdateButton';
import {
  isText,
  isEmail,
  isPhoneNumber,
  isPostalCode,
} from 'profile-page/validators/inputValidators';

const ProfileEditFormWrapper = styled.div.attrs({ className: 'profile-edit-form-wrapper' })`
  margin-top: 20px;
`;

const ProfileGridWrapper = styled.div.attrs({ className: 'profile-grid-wrapper' })`
  padding: 0 20px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-areas: 'profile-form-left profile-form-right';
`;

const ProfileFormLeft = styled.div.attrs({ className: 'profile-form-left' })`
  grid: profile-form-left;
`;

const ProfileFormRight = styled.div.attrs({ className: 'profile-form-right' })`
  grid: profile-form-right;
`;

const selectOptions = ['USA', 'Poland', 'Germany'];

const ProfileEditForm = () => (
  <ProfileEditFormWrapper>
    <ProfileGridWrapper>
      <ProfileFormLeft>
        <TextInput id="first-name-input" label="First Name" validator={isText} />
        <TextInput id="phone-number-input" label="Phone number" validator={isPhoneNumber} />
        <SelectInput options={selectOptions} id="city-select" label="City" />
        <TextInput id="post-codeinput" label="Post Code" validator={isPostalCode} />
      </ProfileFormLeft>
      <ProfileFormRight>
        <TextInput id="last-name-input" label="Last Name" validator={isText} />
        <TextInput id="email-address-input" label="Email address" validator={isEmail} />
        <TextInput id="street-name-input" label="Street" validator={isText} />
        <SelectInput options={selectOptions} id="country-select" label="Country" />
      </ProfileFormRight>
    </ProfileGridWrapper>

    <UpdateButton />
  </ProfileEditFormWrapper>
);

export default ProfileEditForm;
